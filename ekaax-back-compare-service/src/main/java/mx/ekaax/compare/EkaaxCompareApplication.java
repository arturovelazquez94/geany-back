package mx.ekaax.compare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan({"mx.ekaax","mx.ekaax.generic"})
public class EkaaxCompareApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkaaxCompareApplication.class, args);
	}
}
