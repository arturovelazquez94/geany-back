package mx.ekaax.compare.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.ekaax.compare.constants.CompareServiceConstants;
import mx.ekaax.constants.CompareConstants;
import mx.ekaax.constants.ConsultConstants;
import mx.ekaax.generic.repository.entity.CompareReport;
import mx.ekaax.generic.repository.entity.ErrorConsultField;
import mx.ekaax.generic.repository.entity.ProductEcommerceConsultMistaken;
import mx.ekaax.generic.repository.entity.ProductEcommerceConsultSucessful;
import mx.ekaax.generic.repository.entity.ReportContent;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.response.ProductEcommerceError;
import mx.ekaax.utils.general.Nullable;
import mx.ekaax.utils.general.helper.EcommerceEnum;
import mx.ekaax.utils.general.helper.ReportCompareEnum;

/**
 * 
 * @author arturo
 *
 */
public class CompareHelper {

	/**
	 * 
	 * @author arturo
	 *
	 */
	public static enum EcommerceProductsList {

		CSV("CSV"), ECOMMERCE("Ecommerce");

		private String type;

		private EcommerceProductsList(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

	}

	/**
	 * 
	 * This method validates if the list of products is not empty of null
	 * 
	 * @param listProducts
	 * @param productType
	 */
	public static void validateoProductsList(List<ModelProduct> listProducts, EcommerceProductsList productType) {

		if (Nullable.isNullOrEmpty(listProducts)) {
			// Se va a añadir un modulo de exepciones personalizadas, se lanza el
			// RuntimException de forma temporal
			String type = "";
			type = productType.getType();

			throw new RuntimeException("No se encontro informacion en el " + type);
		}
	}

	/**
	 * This method validates the existence of the product in the ecommerce, in such
	 * case the error is store in a list of errors to be displayed to the user
	 * 
	 * @param productErrors
	 * @param productPresent
	 * @param productsFoundEcommerce
	 */
	public static Boolean ecommerceProductExistenceError(List<ProductEcommerceError> productErrors,
			ModelProduct productPresent, List<ModelProduct> productsFoundEcommerce) {

		ProductEcommerceError productError = new ProductEcommerceError();
		boolean present = productsFoundEcommerce.contains(productPresent);

		if (!present) {
			productError.setSku(productPresent.getSkuId());
			productError.setStatus(CompareConstants.COMPARE_PRODUCT_EMPTY);
			productErrors.add(productError);
		}
		return present;
	}

	/**
	 * 
	 * @param successfulProducts
	 * @param product
	 */

	public static void buildCorrectReportObject(List<ProductEcommerceConsultSucessful> successfulProducts,
			ModelProduct product) {

		ProductEcommerceConsultSucessful productSuccessful = new ProductEcommerceConsultSucessful();
		String sku = product.getSkuId();

		productSuccessful.setSku(sku);
		productSuccessful.setProductStatus(CompareConstants.COMPARE_PRODUCT_OK);
		successfulProducts.add(productSuccessful);
	}

	/**
	 * 
	 * @param mistakenProducts
	 * @param productEcommerce
	 */
	public static void buildErrorReportObject(List<ProductEcommerceConsultMistaken> mistakenProducts,
			ModelProduct productEcommerce, ModelProduct productCorrect) {

		ProductEcommerceConsultMistaken productMistaken = new ProductEcommerceConsultMistaken();
		List<ErrorConsultField> fields = new ArrayList<>();
		String sku = productCorrect.getSkuId();

		// Only for ecommerce products found
		if (Nullable.isNotNull(productEcommerce)) {
			productMistaken.setProductStatus(CompareConstants.COMPARE_PRODUCT_MISTAKEN);
			// Title Field
			if (productEcommerce.isbTitle() || Nullable.isNull(productEcommerce.getTitle())) {
				ErrorConsultField titleField = new ErrorConsultField();
				titleField.setFieldName(ConsultConstants.CONSULT_FIELD_TITLE);

				if (productEcommerce.isbTitle()) {

					titleField.setStatus(CompareConstants.COMPARE_PRODUCT_FIELD_NOT_MATCH);
					titleField.setEcommerceFound(productEcommerce.getTitle());
					titleField.setCorrect(productCorrect.getTitle());
				} else if (Nullable.isNull(productEcommerce.getTitle())) {
					titleField.setStatus(CompareConstants.COMPARE_PRODUCT_FIELD_EMPTY);
				}
				fields.add(titleField);
			}
			// Description field
			if (productEcommerce.isbDescription() || Nullable.isNull(productEcommerce.getDescription())) {

				ErrorConsultField descriptionField = new ErrorConsultField();
				descriptionField.setFieldName(ConsultConstants.CONSULT_FIELD_DESCRIPTION);

				if (productEcommerce.isbDescription()) {

					descriptionField.setEcommerceFound(productEcommerce.getDescription());
					descriptionField.setCorrect(productCorrect.getDescription());
					descriptionField.setStatus(CompareConstants.COMPARE_PRODUCT_FIELD_NOT_MATCH);
				} else if (Nullable.isNull(productEcommerce.getDescription())) {
					descriptionField.setStatus(CompareConstants.COMPARE_PRODUCT_FIELD_EMPTY);
				}
				fields.add(descriptionField);
			}
			// Price field

			if (productEcommerce.isbBasePrice() || Nullable.isNull(productEcommerce.getPrice())) {
				ErrorConsultField priceField = new ErrorConsultField();
				priceField.setFieldName(ConsultConstants.CONSULT_FIELD_PRICE);

				if (productEcommerce.isbBasePrice()) {
					priceField.setStatus(CompareConstants.COMPARE_PRODUCT_FIELD_NOT_MATCH);
					priceField.setEcommerceFound(productEcommerce.getPrice().toString());
					priceField
							.setCorrect("[" + productCorrect.getMinPrice() + "," + productCorrect.getMaxPrice() + "]");

				} else if (Nullable.isNull(productEcommerce.getPrice())) {
					priceField.setStatus(CompareConstants.COMPARE_PRODUCT_FIELD_EMPTY);
				}
				fields.add(priceField);
			}
		} else {
			// For products that were not found in the ecommerce
			productMistaken.setProductStatus(CompareConstants.COMPARE_PRODUCT_EMPTY);
		}
		productMistaken.setSku(sku);
		productMistaken.setFields(fields);

		mistakenProducts.add(productMistaken);

	}

	/**
	 * 
	 * @param ecommerce
	 * @param reportCompareEnum
	 * @param successfulProducts
	 * @param mistakenProducts
	 * @return
	 */
	public static CompareReport buildConsultReport(EcommerceEnum ecommerce, ReportCompareEnum reportCompareEnum,
			List<ProductEcommerceConsultSucessful> successfulProducts,
			List<ProductEcommerceConsultMistaken> mistakenProducts) {

		CompareReport compareReport = new CompareReport();
		ReportContent reportContent = new ReportContent();

		Date reportDate = new Date();
		Integer compareType = reportCompareEnum.getCompareType();
		Integer ecommerceType = ecommerce.getIdEcommerce();
		System.out.println("ECOMMERCE TYPE:::::" + ecommerceType);

		compareReport.setReportDate(reportDate);
		compareReport.setCompareType(compareType);
		compareReport.setEcommerce(ecommerceType);

		reportContent.setMistakenProducts(mistakenProducts);
		reportContent.setSuccessfulProducts(successfulProducts);
		reportContent.setTotalSuccessfulProducts(successfulProducts.size());
		reportContent.setTotalMistakenProducts(mistakenProducts.size());

		compareReport.setContent(reportContent);

		return compareReport;

	}

	public static Boolean compareProducts(ReportCompareEnum compare, ModelProduct ecommerceProductCompare,
			ModelProduct ecommerceProductCorrect, Boolean correctProduct) {

		// Title
		String correctTitle = ecommerceProductCorrect.getTitle();
		String ecommerceTitle = ecommerceProductCompare.getTitle();

		if ((Nullable.isNotNull(ecommerceTitle) && Nullable.isNotNull(correctTitle))
				&& (correctTitle.equals(ecommerceTitle))) {
			// Incorrect
			ecommerceProductCompare.setbTitle(false);

		} else {
			ecommerceProductCompare.setbTitle(true);
			correctProduct = false;
		}

		// Price
		BigDecimal correctMaxPrice = ecommerceProductCorrect.getMaxPrice();
		BigDecimal correctMinPrice = ecommerceProductCorrect.getMinPrice();

		BigDecimal ecommercePrice = ecommerceProductCompare.getPrice();

		if (Nullable.isNotNull(ecommercePrice)) {
			if (!compare.equals(ReportCompareEnum.ECOMMERCE)) {
				if (ecommercePrice.compareTo(correctMaxPrice) == 1 || ecommercePrice.compareTo(correctMinPrice) == -1) {
					// Out of the range
					ecommerceProductCompare.setbBasePrice(true);
					correctProduct = false;

				} else {
					ecommerceProductCompare.setbBasePrice(false);

				}
			} else {
				if ((Nullable.isNotNull(ecommercePrice)) && (Nullable.isNotNull(ecommerceProductCorrect.getPrice())
						&& (ecommercePrice.compareTo(ecommerceProductCorrect.getPrice()) == 0))) {
					// Correct
					ecommerceProductCompare.setbBasePrice(false);

				} else {
					ecommerceProductCompare.setbBasePrice(true);
					correctProduct = false;

				}
			}

		} else {
			ecommerceProductCompare.setbBasePrice(false);

		}

		// Description
		String correctDescription = ecommerceProductCorrect.getDescription();
		String ecommerceDescription = ecommerceProductCompare.getDescription();

		if ((Nullable.isNotNull(correctDescription) && Nullable.isNotNull(ecommerceDescription))
				&& (correctDescription.equals(ecommerceDescription))) {

			ecommerceProductCompare.setbDescription(false);

		} else {
			ecommerceProductCompare.setbDescription(true);
			correctProduct = false;

		}

		return correctProduct;
	}

}
