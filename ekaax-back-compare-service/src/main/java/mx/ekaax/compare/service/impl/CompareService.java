package mx.ekaax.compare.service.impl;

import static mx.ekaax.compare.helper.CompareHelper.buildCorrectReportObject;
import static mx.ekaax.compare.helper.CompareHelper.buildErrorReportObject;
import static mx.ekaax.compare.helper.CompareHelper.ecommerceProductExistenceError;
import static mx.ekaax.compare.helper.CompareHelper.validateoProductsList;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ekaax.compare.helper.CompareHelper;
import mx.ekaax.compare.helper.CompareHelper.EcommerceProductsList;
import mx.ekaax.compare.service.ICompareService;
import mx.ekaax.generic.repository.IProductRepository;
import mx.ekaax.generic.repository.IStatisticsRepository;
import mx.ekaax.generic.repository.entity.CompareReport;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.generic.repository.entity.ProductEcommerceConsultMistaken;
import mx.ekaax.generic.repository.entity.ProductEcommerceConsultSucessful;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.model.ProductMapper;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.CompareModelsResponse;
import mx.ekaax.response.ProductEcommerceError;
import mx.ekaax.utils.general.Nullable;
import mx.ekaax.utils.general.helper.EcommerceEnum;
import mx.ekaax.utils.general.helper.ReportCompareEnum;

@Service
public class CompareService implements ICompareService {

	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private IStatisticsRepository statisticsRepository;

	@Autowired
	private IProductRepository productRepository;

	@Autowired
	private ProductMapper productMapper;

	private ModelProduct productBase;

	private ModelProduct productCompare;

	@Override
	public CompareModelsResponse compareProductDB(CompareModelsRequest modelProductsRequest) {
		CompareModelsResponse compareModelResponse = new CompareModelsResponse();
		logger.info("COMPARE MODEL START >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");

		productBase = modelProductsRequest.getModelBase();
		productCompare = modelProductsRequest.getModelCompare();

		compareProductWithDatabase();

		compareModelResponse.setModelBase(productBase);
		compareModelResponse.setModelCompare(productCompare);

		logger.info(
				"COMPARE MODEL END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ");

		return compareModelResponse;

	}

	@Override
	public CompareModelsResponse compareProductsEcom(CompareModelsRequest modelProductsRequest) {

		CompareModelsResponse compareModelResponse = new CompareModelsResponse();

		productBase = modelProductsRequest.getModelBase();
		productCompare = modelProductsRequest.getModelCompare();

		compareProductsEcommerce();

		compareModelResponse.setModelBase(productBase);
		compareModelResponse.setModelCompare(productCompare);

		return compareModelResponse;
	}

	@Override
	public CompareFileEcommerceResponse compareCsvEcom(List<ModelProduct> listProdCSV, List<ModelProduct> listProdEcomm,
			String[] idEcommerce) {

		CompareFileEcommerceResponse compareFileResponse = new CompareFileEcommerceResponse();

		List<ModelProduct> modelProducts = new ArrayList<>();
		List<ProductEcommerceError> productErrors = new ArrayList<>();

		// Data list validation
		validateoProductsList(listProdCSV, EcommerceProductsList.CSV);
		validateoProductsList(listProdEcomm, EcommerceProductsList.ECOMMERCE);
		// Products to be stored in database
		List<ProductEcommerceConsultSucessful> successfulProducts = new ArrayList<>();
		List<ProductEcommerceConsultMistaken> mistakenProducts = new ArrayList<>();

		// Ecommerce
		String ecommerceType = null;
		System.out.println(":::::::::::::.ECOMMERCE:::::::::::::::::::"+idEcommerce[0]);

		if (!Nullable.isNullOrEmpty(idEcommerce)) {
			ecommerceType = idEcommerce[0];
		}

		EcommerceEnum ecommerce = EcommerceEnum.getEcommerce(ecommerceType);

		listProdCSV.stream().forEach(ecommerceProductCorrect -> {

			// Validate existance of the product in the file
			Boolean present = ecommerceProductExistenceError(productErrors, ecommerceProductCorrect, listProdEcomm);

			// Saves the product if it does not exists in the database
			saveNotExistingProduct(ecommerceProductCorrect);

			// Generate Compare object report for objects not found
			if (!present) {
				buildErrorReportObject(mistakenProducts, null, ecommerceProductCorrect);
			}

			listProdEcomm.stream().forEach(ecommerceProductCompare -> {

				if (Nullable.isNotNull(ecommerceProductCompare)) {

					Boolean correctProduct = true;

					// This block of validation will have to be decoupled
					// ------------------------------------------------------------------------//
					String skuProductCorrect = ecommerceProductCorrect.getSkuId();
					String skStringCompare = ecommerceProductCompare.getSkuId();

					if (skuProductCorrect.equals(skStringCompare)) {
						// Building the output object and comparing the correct product against the
						// ecommerce product

						correctProduct = CompareHelper.compareProducts(ReportCompareEnum.CSV, ecommerceProductCompare,
								ecommerceProductCorrect, correctProduct);

						// Build the product status(Correct-Mistaken)
						if (correctProduct.equals(true)) {

							buildCorrectReportObject(successfulProducts, ecommerceProductCompare);

						} else {
							buildErrorReportObject(mistakenProducts, ecommerceProductCompare, ecommerceProductCorrect);
						}

						// ------------------------------------------------------------------------//
						modelProducts.add(ecommerceProductCompare);
					}
				}

			});

		});

		CompareReport compareReport = CompareHelper.buildConsultReport(ecommerce, ReportCompareEnum.CSV,
				successfulProducts, mistakenProducts);

		saveComparation(compareReport);

		compareFileResponse.setModelProducts(modelProducts);
		compareFileResponse.setProductErrors(productErrors);

		return compareFileResponse;
	}

	public void compareProductWithDatabase() {
		// ============== COMPARACION ============== (VALIDAR CON PREDICADOS DE JAVA 8 )

		// Products to be stored in database
		List<ProductEcommerceConsultSucessful> successfulProducts = new ArrayList<>();
		List<ProductEcommerceConsultMistaken> mistakenProducts = new ArrayList<>();
		Boolean correctProduct = true;

		// Building the output object and comparing the correct product against the
		// ecommerce product

		correctProduct = CompareHelper.compareProducts(ReportCompareEnum.DATABASE, productCompare, productBase,
				correctProduct);

		// Build the product status(Correct-Mistaken)
		if (correctProduct.equals(true)) {

			buildCorrectReportObject(successfulProducts, productCompare);

		} else {
			buildErrorReportObject(mistakenProducts, productCompare, productBase);
		}

		// Se va a refactorizar en un futuro para que quede desacoplada la logica del
		// método
		String ecommerceType = productCompare.getEcommerce();
		EcommerceEnum ecommerce = EcommerceEnum.getEcommerce(ecommerceType);

		CompareReport compareReport = CompareHelper.buildConsultReport(ecommerce, ReportCompareEnum.DATABASE,
				successfulProducts, mistakenProducts);

		saveComparation(compareReport);

	}

	public void compareProductsEcommerce() {

		// Products to be stored in database
		List<ProductEcommerceConsultSucessful> successfulProducts = new ArrayList<>();
		List<ProductEcommerceConsultMistaken> mistakenProducts = new ArrayList<>();
		Boolean correctProduct = true;

		// Building the output object and comparing the correct product against the
		// ecommerce product

		correctProduct = CompareHelper.compareProducts(ReportCompareEnum.ECOMMERCE, productCompare, productBase,
				correctProduct);

		// Build the product status(Correct-Mistaken)
		if (correctProduct.equals(true)) {

			buildCorrectReportObject(successfulProducts, productCompare);

		} else {
			buildErrorReportObject(mistakenProducts, productCompare, productBase);
		}
		// Se va a refactorizar en un futuro para que quede desacoplada la logica del
		// método
		String ecommerceType = productCompare.getEcommerce();
		EcommerceEnum ecommerce = EcommerceEnum.getEcommerce(ecommerceType);

		CompareReport compareReport = CompareHelper.buildConsultReport(ecommerce, ReportCompareEnum.ECOMMERCE,
				successfulProducts, mistakenProducts);

		saveComparation(compareReport);

	}

	@Override
	public void saveComparation(CompareReport compareReport) {
		statisticsRepository.save(compareReport);
	}

	@Override
	public void saveNotExistingProduct(ModelProduct product) {

		if (Nullable.isNotNull(product)) {

			String skuId = product.getSkuId();
			Product productCollection = productRepository.findBySkuId(skuId);

			if (Nullable.isNull(productCollection)) {
				productRepository.save(productMapper.modelToEntity(product));
			}

		}
	}

}
