package mx.ekaax.compare.service;

import java.util.List;

import mx.ekaax.generic.repository.entity.CompareReport;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.CompareModelsResponse;

public interface ICompareService {

	// public ModelProduct findByUpcSku(String upcSku) throws Exception;

	CompareModelsResponse compareProductDB(CompareModelsRequest modelProductsRequest);

	CompareModelsResponse compareProductsEcom(CompareModelsRequest modelProductsRequest);

	CompareFileEcommerceResponse compareCsvEcom(List<ModelProduct> listProdCSV, List<ModelProduct> listProdEcomm,
			String[] idEcommerce);

	void saveComparation(CompareReport consultError);

	void saveNotExistingProduct(ModelProduct product);
}
