package mx.ekaax.generic.repository.entity;

import lombok.Data;

@Data
public class ProductEcommerceConsult {

	private String sku;

	private Integer productStatus;

}
