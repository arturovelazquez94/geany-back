package mx.ekaax.generic.repository.entity;

import java.util.List;

import lombok.Data;

@Data
public class ProductEcommerceConsultMistaken extends ProductEcommerceConsult {

	private List<ErrorConsultField> fields;
}
