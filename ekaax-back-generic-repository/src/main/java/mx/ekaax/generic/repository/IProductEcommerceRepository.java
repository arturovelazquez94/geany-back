package mx.ekaax.generic.repository;

import mx.ekaax.generic.repository.entity.ProductEcommerce;

public interface IProductEcommerceRepository extends IMongoRepositoryProductEcomm {
	
	public ProductEcommerce findProductBySkuId(String skuId);

}
