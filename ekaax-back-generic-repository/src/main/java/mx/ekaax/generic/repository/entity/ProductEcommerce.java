package mx.ekaax.generic.repository.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ProductEcommerce")
public class ProductEcommerce implements Serializable {
	private static final long serialVersionUID = 1422536844746235712L;

	@Id
	private String id;

	private Integer idEcommerce;

	private String skuId;

	private String upcId;

	private String title;

	private String summary;

	private String description;

	private BigDecimal minPrice;

	private BigDecimal price;

	private BigDecimal maxPrice;

	private String picture;

	private String promotion;

	public ProductEcommerce() {
		super();
	}

	public ProductEcommerce(String id, Integer idEcommerce, String skuId, String upcId, String title, String summary,
			String description, BigDecimal minPrice, BigDecimal price, BigDecimal maxPrice, String picture,
			String promotion) {
		super();
		this.id = id;
		this.idEcommerce = idEcommerce;
		this.skuId = skuId;
		this.upcId = upcId;
		this.title = title;
		this.summary = summary;
		this.description = description;
		this.minPrice = minPrice;
		this.price = price;
		this.maxPrice = maxPrice;
		this.picture = picture;
		this.promotion = promotion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getUpcId() {
		return upcId;
	}

	public void setUpcId(String upcId) {
		this.upcId = upcId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Integer getIdEcommerce() {
		return idEcommerce;
	}

	public void setIdEcommerce(Integer idEcommerce) {
		this.idEcommerce = idEcommerce;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductEcommerce other = (ProductEcommerce) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductEcommerce [id=" + id + ", idEcommerce=" + idEcommerce + ", skuId=" + skuId + ", upcId=" + upcId
				+ ", title=" + title + ", summary=" + summary + ", description=" + description + ", minPrice="
				+ minPrice + ", price=" + price + ", maxPrice=" + maxPrice + ", picture=" + picture + ", promotion="
				+ promotion + "]";
	}

}
