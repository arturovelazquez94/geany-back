package mx.ekaax.generic.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.ekaax.generic.repository.entity.CompareReport;

@Repository
public interface IReportRepository extends MongoRepository<CompareReport, String> {

	Page<CompareReport> findByReportDateBetween(Date dateStart, Date dateEnd, Pageable pageable);

}
