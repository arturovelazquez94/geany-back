package mx.ekaax.generic.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.ekaax.generic.repository.entity.Product;

@Repository
public interface IMongoRepositoryProduct extends MongoRepository<Product, String> {

	Product findBySkuId(String skuId);
	

}
