package mx.ekaax.generic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.ekaax.generic.repository.entity.CompareReport;

@Repository
public interface IStatisticsRepository extends MongoRepository<CompareReport, String> {

}
