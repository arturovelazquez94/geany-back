package mx.ekaax.generic.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.ekaax.generic.repository.entity.ProductEcommerce;

@Repository
public interface IMongoRepositoryProductEcomm extends MongoRepository<ProductEcommerce, String> {

	ProductEcommerce findBySkuIdAndIdEcommerce(String skuId, Integer idEcommerce);

	List<ProductEcommerce> findByIdEcommerce(Integer idEcommerce);

}
