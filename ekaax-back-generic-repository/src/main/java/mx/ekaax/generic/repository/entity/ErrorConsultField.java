package mx.ekaax.generic.repository.entity;

import lombok.Data;

@Data
public class ErrorConsultField {

	private String fieldName;

	private Integer status;

	private String correct;

	private String ecommerceFound;

}
