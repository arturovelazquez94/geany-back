package mx.ekaax.generic.repository.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "CompareReport")
@JsonInclude(Include.NON_NULL)
public class CompareReport {

	@Id
	private String reportId;

	@Field("reportDate")
	@JsonFormat(shape=Shape.NUMBER_FLOAT)
	private Date reportDate;

	@Field("ecommerce")
	private Integer ecommerce;

	@Field("compareType")
	private Integer compareType;

	@Field("userId")
	private String userId;

	@Field("content")
	private ReportContent content;

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public Integer getEcommerce() {
		return ecommerce;
	}

	public void setEcommerce(Integer ecommerce) {
		this.ecommerce = ecommerce;
	}

	public Integer getCompareType() {
		return compareType;
	}

	public void setCompareType(Integer compareType) {
		this.compareType = compareType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public ReportContent getContent() {
		return content;
	}

	public void setContent(ReportContent content) {
		this.content = content;
	}

}
