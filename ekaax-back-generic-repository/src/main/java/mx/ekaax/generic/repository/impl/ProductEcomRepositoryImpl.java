package mx.ekaax.generic.repository.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import mx.ekaax.generic.repository.IProductEcommerceRepository;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.generic.repository.entity.ProductEcommerce;

@Repository
public abstract class ProductEcomRepositoryImpl implements IProductEcommerceRepository{
	
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public ProductEcommerce findProductBySkuId(String skuId) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("skuId").is(skuId));
			
//			Criteria criteria = new Criteria();
//	        criteria
//	        	.orOperator(
//	        		Criteria.where("skuId").is(skuId.getSkuId()),
//	        		Criteria.where("upcId").is(prodEcomm.getUpcId())
//	        		)
//	        	.andOperator(
//	        			Criteria.where("idEcommerce").is(prodEcomm.getIdEcommerce())
//	        			);
//	        Query query = new Query(criteria);
			
			return mongoTemplate.findOne(query, ProductEcommerce.class);
		} catch (Exception e) {
			logger.error("DAO: "+e);
			return null;
		}
	}
	
}
