package mx.ekaax.generic.repository.entity;

import java.util.List;

import lombok.Data;

@Data
public class ReportContent {

	private List<ProductEcommerceConsultSucessful> successfulProducts;

	private List<ProductEcommerceConsultMistaken> mistakenProducts;

	private Integer totalSuccessfulProducts;

	private Integer totalMistakenProducts;

}
