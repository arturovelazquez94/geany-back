package mx.ekaax.statistics.helper;

import java.math.BigDecimal;

import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.generic.repository.entity.ProductEcommerce;
import mx.ekaax.response.EcommercePriceProduct;

public class StatisticsHelper {

	/**
	 * This method is used to build the Price-report object for the specific product
	 * 
	 * @param correctProduct
	 * @param ecommerceProduct
	 * @return
	 */
	public static EcommercePriceProduct buildEcommercePriceProduct(Product correctProduct,
			ProductEcommerce ecommerceProduct) {

		EcommercePriceProduct priceProduct = new EcommercePriceProduct();

		priceProduct.setSkuId(correctProduct.getSkuId());

		// Price
		BigDecimal upperBound = correctProduct.getMaxPrice();
		BigDecimal lowerBound = correctProduct.getMinPrice();

		BigDecimal price = ecommerceProduct.getPrice();

		if (price.compareTo(lowerBound) == -1 || price.compareTo(upperBound) == 1) {
			// Incorrect
			Integer gapStatus = 0;
			BigDecimal gapDifference = null;
			if (price.compareTo(upperBound) == 1) {
				gapStatus = 1;
				gapDifference = price.subtract(upperBound);
			} else if (price.compareTo(lowerBound) == -1) {
				gapStatus = 2;
				gapDifference = lowerBound.subtract(price);

			}

			priceProduct.setGapType(gapStatus);
			priceProduct.setGapQuantity(gapDifference);
		}
		priceProduct.setEcommercePrice(price);
		priceProduct.setUpperPrice(upperBound);
		priceProduct.setLowerPrice(lowerBound);

		return priceProduct;
	}

}
