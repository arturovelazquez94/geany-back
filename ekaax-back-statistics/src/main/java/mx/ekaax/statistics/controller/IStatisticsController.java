package mx.ekaax.statistics.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ekaax.request.ReportRequest;

@RestController
@RequestMapping("/reports")
public interface IStatisticsController {

	@PostMapping("/comparations")
	ResponseEntity<?> getCompareReport(ReportRequest reportRequest, Pageable pageable);

	@PostMapping("/prices")
	ResponseEntity<?> getPriceReport(ReportRequest reportRequest);

	@GetMapping("/products")
	ResponseEntity<?> getEcommerceSKUSReport();

}
