package mx.ekaax.statistics.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import mx.ekaax.request.ReportRequest;
import mx.ekaax.statistics.controller.IStatisticsController;
import mx.ekaax.statistics.service.IStatisticsServiceWrapper;

@Component
public class StatisticsControllerImpl implements IStatisticsController {

	@Autowired
	private IStatisticsServiceWrapper statisticsServiceWrapper;

	// Refactorizar e implementar un strategy pattern y solamente tener un sólo
	// método y mandarf todo en el RequestBody
	@Override
	public ResponseEntity<?> getCompareReport(@RequestBody ReportRequest reportRequest, Pageable pageable) {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(statisticsServiceWrapper.getCompareReportByDates(reportRequest, pageable));
	}

	@Override
	public ResponseEntity<?> getEcommerceSKUSReport() {
		return ResponseEntity.ok(statisticsServiceWrapper.getProductsByEcommerceReport());
	}

	@Override
	public ResponseEntity<?> getPriceReport(@RequestBody ReportRequest reportRequest) {
		System.out.println("ENTRANCE::::::::::::::::::::" + reportRequest.getEcommerce());
		System.out.println("ENTRANCE2::::::::::::::::::::" + reportRequest);

		return ResponseEntity.ok(statisticsServiceWrapper.getPriceReport(reportRequest));
	}

}
