package mx.ekaax.statistics.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import mx.ekaax.request.ReportRequest;
import mx.ekaax.response.CompareReportResponse;
import mx.ekaax.response.EcommercePriceReport;
import mx.ekaax.response.EcommerceProductReportResponse;
import mx.ekaax.statistics.service.IStatisticsService;
import mx.ekaax.statistics.service.IStatisticsServiceWrapper;

@Service
public class StatisticsServiceWrapperImpl implements IStatisticsServiceWrapper {

	@Autowired
	private IStatisticsService statisticsService;

	@Override
	public CompareReportResponse getCompareReportByDates(ReportRequest reportRequest, Pageable pageable) {
		CompareReportResponse compareReport = new CompareReportResponse();
		compareReport.setReports(statisticsService
				.getCompareReportByDates(reportRequest.getDateStart(), reportRequest.getDateEnd(), pageable)
				.getContent());

		return compareReport;
	}

	@Override
	public EcommerceProductReportResponse getProductsByEcommerceReport() {
		return statisticsService.getProductsByEcommerceReport();
	}

	@Override
	public EcommercePriceReport getPriceReport(ReportRequest reportRequest) {
		return statisticsService.getEcommercePriceReport(reportRequest.getEcommerce());
	}

}
