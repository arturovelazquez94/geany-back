package mx.ekaax.statistics.service;

import org.springframework.data.domain.Pageable;

import mx.ekaax.request.ReportRequest;
import mx.ekaax.response.CompareReportResponse;
import mx.ekaax.response.EcommercePriceReport;
import mx.ekaax.response.EcommerceProductReportResponse;

public interface IStatisticsServiceWrapper {

	CompareReportResponse getCompareReportByDates(ReportRequest reportRequest, Pageable pageable);

	EcommerceProductReportResponse getProductsByEcommerceReport();

	EcommercePriceReport getPriceReport(ReportRequest reportRequest);

}
