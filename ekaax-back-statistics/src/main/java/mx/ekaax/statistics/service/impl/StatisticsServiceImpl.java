package mx.ekaax.statistics.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import mx.ekaax.generic.repository.IProductEcommerceRepository;
import mx.ekaax.generic.repository.IProductRepository;
import mx.ekaax.generic.repository.IReportRepository;
import mx.ekaax.generic.repository.entity.CompareReport;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.generic.repository.entity.ProductEcommerce;
import mx.ekaax.model.ProductMapper;
import mx.ekaax.response.EcommercePrice;
import mx.ekaax.response.EcommercePriceProduct;
import mx.ekaax.response.EcommercePriceReport;
import mx.ekaax.response.EcommerceProductReportResponse;
import mx.ekaax.response.EcommerceReportResponse;
import mx.ekaax.response.ModelProductResponse;
import mx.ekaax.statistics.helper.StatisticsHelper;
import mx.ekaax.statistics.service.IStatisticsService;
import mx.ekaax.utils.general.Nullable;
import mx.ekaax.utils.general.helper.EcommerceEnum;

@Service
public class StatisticsServiceImpl implements IStatisticsService {

	@Autowired
	private IProductRepository productRepository;

	@Autowired
	private IReportRepository reportRepository;

	@Autowired
	private IProductEcommerceRepository productEcommerceRepository;

	@Autowired
	private ProductMapper productMapper;

	@Override
	public Page<CompareReport> getCompareReportByDates(Date dateStart, Date dateEnd, Pageable pageable) {
		return reportRepository.findByReportDateBetween(dateStart, dateEnd, pageable);
	}

	@Override
	public EcommerceProductReportResponse getProductsByEcommerceReport() {

		EcommerceProductReportResponse ecommerceProductReport = new EcommerceProductReportResponse();

		List<EcommerceReportResponse> ecommerce = new ArrayList<>();

		// -----------------------------Refactorizar Repository
		// layer---------------------------------------
		// Actualmente se obtienen todos los campos, se tiene que cambiar
		// por una agregacion de mongo indicandole la salida, de esta manera no devolver
		// todos los campos de los productos, temporalmente se esta mappeando el objeto
		// mediante MapStructs

		// CHEDRAUI

		ecommerce.add(buildEcommerceReport(EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_CHEDRAUI));

		// WALMART

		ecommerce.add(buildEcommerceReport(EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_WALMART));

		// SUPERAMA

		ecommerce.add(buildEcommerceReport(EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_SUPERAMA));

		ecommerceProductReport.setEcommerce(ecommerce);

		// TODO Auto-generated method stub
		return ecommerceProductReport;
	}

	@Override
	public EcommerceReportResponse buildEcommerceReport(Integer ecommerce) {

		EcommerceReportResponse ecommerceReport = new EcommerceReportResponse();
		List<ProductEcommerce> productsEcommerce = productEcommerceRepository.findByIdEcommerce(ecommerce);

		List<ModelProductResponse> productsEcommerceReponse = productMapper
				.procuctEcommerceToResponse(productsEcommerce);

		ecommerceReport.setEcommerce(ecommerce);
		ecommerceReport.setProducts(productsEcommerceReponse);
		ecommerceReport.setTotalSkus(productsEcommerceReponse.size());

		return ecommerceReport;
	}

	@Override
	public EcommercePriceReport getEcommercePriceReport(Integer[] ecommerce) {

		// Correct products
		List<Product> productsCorrect = productRepository.findAll();
		List<EcommercePrice> ecommercePrices = new ArrayList<>();
		List<Integer> ecommerceType = null;

		if (Nullable.isNotNull(ecommerce)) {
			ecommerceType = Arrays.asList(ecommerce);
		}

		// CHEDRAUI
		if (Nullable.isNull(ecommerceType)
				|| ecommerceType.contains(EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_CHEDRAUI)) {

			ecommercePrices
					.add(buildEcommercePrice(productsCorrect, EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_CHEDRAUI));
		}
		// WALMART
		if (Nullable.isNull(ecommerceType)
				|| ecommerceType.contains(EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_WALMART)) {

			ecommercePrices
					.add(buildEcommercePrice(productsCorrect, EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_WALMART));
		}
		// SUPERAMA
		if (Nullable.isNull(ecommerceType)
				|| ecommerceType.contains(EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_WALMART)) {

			ecommercePrices
					.add(buildEcommercePrice(productsCorrect, EcommerceEnum.Constants.CONSULT_ECOMMERCE_TYPE_SUPERAMA));
		}
		// Ecomemrce Products
		EcommercePriceReport ecommercePriceReport = new EcommercePriceReport();

		ecommercePriceReport.setEcommerce(ecommercePrices);

		return ecommercePriceReport;
	}

	@Override
	public EcommercePrice buildEcommercePrice(List<Product> correctProducts, Integer ecommerce) {

		List<ProductEcommerce> productsEcommerce = productEcommerceRepository.findByIdEcommerce(ecommerce);

		List<EcommercePriceProduct> ecommercePriceProducst = new ArrayList<>();
		EcommercePrice ecommercePrice = new EcommercePrice();

		productsEcommerce.stream().forEach(productIterator -> {

			correctProducts.stream().forEach(correctProductIterator -> {

				if (productIterator.getSkuId().equals(correctProductIterator.getSkuId())) {

					ecommercePriceProducst
							.add(StatisticsHelper.buildEcommercePriceProduct(correctProductIterator, productIterator));
				}
			});
		});

		ecommercePrice.setEcommerce(ecommerce);
		ecommercePrice.setProducts(ecommercePriceProducst);

		return ecommercePrice;
	}

}
