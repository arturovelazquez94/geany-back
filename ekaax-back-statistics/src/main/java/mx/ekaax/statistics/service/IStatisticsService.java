package mx.ekaax.statistics.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mx.ekaax.generic.repository.entity.CompareReport;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.response.EcommercePrice;
import mx.ekaax.response.EcommercePriceReport;
import mx.ekaax.response.EcommerceProductReportResponse;
import mx.ekaax.response.EcommerceReportResponse;

/**
 * 
 * @author Arturo Velázquez
 *
 */
public interface IStatisticsService {
	/**
	 * This method is used to retrieve the compare-report from database based on the
	 * date range[dateStart,dateEnd]
	 * 
	 * @param dateStart
	 * @param dateEnd
	 * @param pageable
	 * @return
	 */
	Page<CompareReport> getCompareReportByDates(Date dateStart, Date dateEnd, Pageable pageable);

	/**
	 * This method is used the generate the products-report(sk's by ecommerce) by
	 * 
	 * @return
	 */
	EcommerceProductReportResponse getProductsByEcommerceReport();

	/**
	 * This method is used to build the ecommerce object based on the ecommerce
	 * 
	 * @param ecommerce
	 * @return
	 */
	EcommerceReportResponse buildEcommerceReport(Integer ecommerce);

	/**
	 * This method is used to build the ecommerce-price report
	 * 
	 * @param ecommerce
	 * @return
	 */
	EcommercePriceReport getEcommercePriceReport(Integer[] ecommerce);

	/**
	 * 
	 * @param correctProducts
	 * @param ecommerce
	 * @return
	 */
	EcommercePrice buildEcommercePrice(List<Product> correctProducts, Integer ecommerce);

}
