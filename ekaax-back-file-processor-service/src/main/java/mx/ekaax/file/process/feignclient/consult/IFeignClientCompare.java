package mx.ekaax.file.process.feignclient.consult;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;

@FeignClient(name = "gateway")
public interface IFeignClientCompare {

	@PostMapping("ekaax/compare-products/ekaax/api/compare/v1/product/csv/ecommerce")
	public CompareFileEcommerceResponse compareProductsCsvEcommerce(
			@RequestBody CompareEcommerceCSVRequest compareEcomCsvReq);

}
