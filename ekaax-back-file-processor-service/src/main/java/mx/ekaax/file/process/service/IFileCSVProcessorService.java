package mx.ekaax.file.process.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import mx.ekaax.response.FileCSVRowResponse;


public interface IFileCSVProcessorService {

	List<FileCSVRowResponse> processFile(MultipartFile file);

}
