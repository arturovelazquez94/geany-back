package mx.ekaax.file.process.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mx.ekaax.file.process.feignclient.consult.IFeignClientCompare;
import mx.ekaax.file.process.feignclient.consult.IFeignClientConsultEcommerce;
import mx.ekaax.file.process.service.IFileCSVProcessorService;
import mx.ekaax.file.process.service.IFileCSVProcessorServiceWrapper;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.model.ProductMapper;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.FileCSVRowResponse;

@Service
public class FileCSVProcessorServiceWrapperImpl implements IFileCSVProcessorServiceWrapper {

	@Autowired
	private IFileCSVProcessorService fileProcessorService;

	@Autowired
	private IFeignClientConsultEcommerce feignClientConsultEcommerce;

	@Autowired
	private IFeignClientCompare feignClientCompare;

	@Autowired
	private ProductMapper productMapper;

	@Override
	public CompareFileEcommerceResponse processFile(MultipartFile file, String ecommerce) {

		CompareFileEcommerceResponse listProductsCSV = null;
		List<ModelProduct> listProductsCSVModel = null;

		// Processing the file
		List<FileCSVRowResponse> csvResponse = fileProcessorService.processFile(file);

		List<String> csvsku = csvResponse.stream().map(responseRow -> responseRow.getSku())
				.collect(Collectors.toList());

		listProductsCSVModel = productMapper.csvResponseToModel(csvResponse);

		String[] ecommerceArray = { ecommerce };

		// Building the request to consume consultEcommerce service
		CompareEcommerceCSVRequest request = new CompareEcommerceCSVRequest();
		request.setIdEcommerce(ecommerceArray);
		request.setListProdCSVString(csvsku);

		// Using Feign as a rest client
		List<ModelProduct> ecommerceProducts = feignClientConsultEcommerce.consultEcommerce(request);

		System.out.println("FETCH CONSULT:::::::::" + " ECOMMERCE" + ecommerce + "::::::::::::" + ecommerceProducts);

		CompareEcommerceCSVRequest compareEcomCsvReq = new CompareEcommerceCSVRequest();

		compareEcomCsvReq.setListProdCSV(listProductsCSVModel);
		compareEcomCsvReq.setListProdEcomm(ecommerceProducts);
		compareEcomCsvReq.setListProdCSVString(csvsku);
		compareEcomCsvReq.setIdEcommerce(new String[] { ecommerce });

		// Getting the comparation result between csv and ecommerce
		listProductsCSV = feignClientCompare.compareProductsCsvEcommerce(compareEcomCsvReq);

		return listProductsCSV;
	}

}
