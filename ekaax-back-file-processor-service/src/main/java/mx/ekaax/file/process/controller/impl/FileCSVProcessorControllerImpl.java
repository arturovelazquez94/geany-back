package mx.ekaax.file.process.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import mx.ekaax.file.process.controller.IFileCSVProcessorController;
import mx.ekaax.file.process.service.IFileCSVProcessorServiceWrapper;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.utils.general.helper.ControllerHelper;

@Component
public class FileCSVProcessorControllerImpl implements IFileCSVProcessorController {

	@Autowired
	private IFileCSVProcessorServiceWrapper fileProcessorWrapper;

	@Override
	public ResponseEntity<?> process(@RequestParam("file") MultipartFile file, @RequestParam String ecommerce) {
		// TODO Auto-generated method stub
		return ControllerHelper.responseOK(fileProcessorWrapper.processFile(file, ecommerce));
	}

	@Override
	public ResponseEntity<?> example() {
		// TODO Auto-generated method stub
		return ControllerHelper.responseOK("SUCCESS");
	}

}
