package mx.ekaax.file.process.feignclient.consult;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareEcommerceCSVRequest;

@FeignClient(name = "gateway")
public interface IFeignClientConsultEcommerce {

	@PostMapping("ekaax/consult-sku/ekaax/api/v1/consulta")
	public List<ModelProduct> consultEcommerce(@RequestBody CompareEcommerceCSVRequest request);

}
