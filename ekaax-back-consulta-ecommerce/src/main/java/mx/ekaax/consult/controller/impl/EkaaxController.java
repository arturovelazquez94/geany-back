package mx.ekaax.consult.controller.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import mx.ekaax.consult.controller.IEkaaxController;
import mx.ekaax.consult.service.IConsultService;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.request.ConsultEcommerceRequest;
import mx.ekaax.response.CompareModelsResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.utils.general.Nullable;
import mx.ekaax.utils.general.helper.ControllerHelper;

@Component
public class EkaaxController implements IEkaaxController {
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private IConsultService consultService;

	@Override
	public ResponseEntity<?> consultProductEkaax(@PathVariable(value = "upcSku", required = true) String upcSku) {
		logger.info("EkaaxController consultProduct >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		ModelProduct modelProduct = consultService.consultProductEkaax(upcSku);
		
		if(Nullable.isNotNull(modelProduct)) {
			return ControllerHelper.responseOK(modelProduct);
		}else {
			return ControllerHelper.responseNotFound(null);
		}
	}
	
	@Override
	public ResponseEntity<?> consultEcommerceProduct(@RequestBody ConsultEcommerceRequest consultEcommerceRequest) {
		ConsultEcommerceResponse cer = consultService.consultProductEcommerce(consultEcommerceRequest);
		if(cer.getModelProducts() != null) {
			return ControllerHelper.responseOK(cer);
		}else {
			return ControllerHelper.responseNotFound(cer);
		}
	}

	@Override
	public ResponseEntity<?> compareProducts(@RequestBody ConsultEcommerceRequest consultEcommerceRequest) {
		
			logger.info("EkaaxController >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+consultEcommerceRequest);
			ConsultEcommerceResponse consultEcommerceResponse;
			CompareModelsResponse compareModelsResponse;
	
			if (consultEcommerceRequest.isCompare()) {
				logger.info("EkaaxController COMPARE WITH DB: ");
				consultEcommerceResponse = consultService.compareDatabaseEcommerce(consultEcommerceRequest);
				if(Nullable.isNull(consultEcommerceResponse.getModelProducts())) {
					return ControllerHelper.responseNotFound(consultEcommerceResponse);
				}
				return ControllerHelper.responseOK(consultEcommerceResponse);
			} else {
				logger.info("EkaaxController COMPARE WITH ECOMMERCE: ");
				compareModelsResponse = consultService.compareEcommerce(consultEcommerceRequest);
				if(compareModelsResponse.getModelBase() == null || compareModelsResponse.getModelCompare() == null) {
					return ControllerHelper.responseNotFound(compareModelsResponse);
				}
				return ControllerHelper.responseOK(compareModelsResponse);
			}
			
	}

	@Override
	public List<ModelProduct> csvConsultEcommController(@RequestBody CompareEcommerceCSVRequest request) {
		System.out.println("-------Entrando a la busqueda---------");
		List<ModelProduct> models = consultService.csvConsultEcommService(request.getListProdCSVString(),
				request.getIdEcommerce());
		logger.info("Controller busqueda::::::::" + models);
		return models;
	}

	@Override
	public ResponseEntity<?> consultTest() {
		return new ResponseEntity<String>("Test Consult Ecommerce", HttpStatus.OK);
	}

}
