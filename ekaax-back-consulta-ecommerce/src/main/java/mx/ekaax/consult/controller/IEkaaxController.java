package mx.ekaax.consult.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.request.ConsultEcommerceRequest;

/**
 * 
 * @author alfonso
 *
 */
@RestController
@RequestMapping("/ekaax/api")
public interface IEkaaxController {
	
	@GetMapping
	public ResponseEntity<?> consultTest();
	
	/**
	 * Method for consult one product in the database's ekaax
	 * 
	 * @param upcSku
	 * @return
	 */
	@GetMapping(value="/v1/consulta/{upcSku}")
	public ResponseEntity<?> consultProductEkaax(String upcSku);
	
	@PostMapping(value="/v1/consulta/ecommerce", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> consultEcommerceProduct(@RequestBody ConsultEcommerceRequest consultEcommerceRequest);
	
	/**
	 * Method for consult one product from the database vs some ecommerce 
	 * 
	 * @param upcSku
	 * @param ecommerce
	 * @param compare flat to know if we are going 
	 * @return
	 */
	@PostMapping(value="/v1/consulta/compare", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> compareProducts(@RequestBody ConsultEcommerceRequest consultEcommerceRequest);

	@PostMapping(value="/v1/consulta")
	public List<ModelProduct> csvConsultEcommController(@RequestBody CompareEcommerceCSVRequest request);
	
}
