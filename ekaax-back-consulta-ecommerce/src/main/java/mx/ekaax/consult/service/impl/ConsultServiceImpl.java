package mx.ekaax.consult.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ekaax.consult.chedraui.service.IChedrauiService;
import mx.ekaax.consult.feignclient.compare.IFeignClientCompareService;
import mx.ekaax.consult.service.IConsultService;
import mx.ekaax.consult.superama.service.ISuperamaService;
import mx.ekaax.consult.walmart.service.IWalmartService;
import mx.ekaax.generic.repository.IProductEcommerceRepository;
import mx.ekaax.generic.repository.IProductRepository;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.generic.repository.entity.ProductEcommerce;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.model.ProductMapper;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.request.ConsultEcommerceRequest;
import mx.ekaax.response.CompareModelsResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ProductEcommerceError;
import mx.ekaax.utils.general.Nullable;
import mx.ekaax.utils.general.helper.EcommerceEnum;

@Service
public class ConsultServiceImpl implements IConsultService {
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private IChedrauiService chedrauiService;

	@Autowired
	private IWalmartService walmartService;
	
	@Autowired 
	private ISuperamaService superamaService;

	@Autowired
	private IFeignClientCompareService feignClientCompareService;

	@Autowired
	private IProductRepository productRepository;
	
	@Autowired
	private IProductEcommerceRepository productEcommerceRepository;

	@Autowired
	private ProductMapper productMapper;

	/**
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::: Consult Product in DataBase :::::::::::::::::::::
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public ModelProduct consultProductEkaax(String upcSku) {
		ModelProduct model;
		Product product = productRepository.findBySkuId(upcSku);
		model = productMapper.entityToModel(product);
		return model;
	}
	
	/**
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::: Consult one product in different Ecommerce ::::::::
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public ConsultEcommerceResponse consultProductEcommerce(ConsultEcommerceRequest consultEcommerceRequest) {
		logger.info("START ConsultServiceImpl Consult one product in different Ecommerce  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		logger.info(consultEcommerceRequest);
		
		ConsultEcommerceResponse consultEcommerceResponse = new ConsultEcommerceResponse();
		ModelProduct modelProd;
		
		for(Integer ecom : consultEcommerceRequest.getEcommerce()) {
			modelProd = null;
			if(ecom.equals(EcommerceEnum.CHEDRAUI.getIdEcommerce())) {
				modelProd = chedrauiService.consultProduct(consultEcommerceRequest.getUpcSku());
			} else if(ecom.equals(EcommerceEnum.WALMART.getIdEcommerce())) {
				modelProd = walmartService.consultProduct(consultEcommerceRequest.getUpcSku());
			} else if(ecom.equals(EcommerceEnum.SUPERAMA.getIdEcommerce())) {
				modelProd = superamaService.consultProduct(consultEcommerceRequest.getMarca(), consultEcommerceRequest.getUpcSku());
			}
			
			if(Nullable.isNotNull(modelProd)) {
				Integer[] ecommerceId = {ecom};
				saveProductEcommerce(modelProd, ecommerceId);
				consultEcommerceResponse.addListModelProduct(modelProd); 
			}
			else { consultEcommerceResponse.addListProductEcommerceError(new ProductEcommerceError(consultEcommerceRequest.getUpcSku(), "Producto no encontrado en el ecommerce")); }
		}
		return consultEcommerceResponse;
	}

	/**
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::: Compare DataBase VS Ecommerce :::::::::::::
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public ConsultEcommerceResponse compareDatabaseEcommerce(ConsultEcommerceRequest consultEcommerceRequest) {
		logger.info("START ConsultServiceImpl DataBase VS Ecommerce  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		List<ModelProduct> listProduct = new ArrayList<>();
		ModelProduct modelProdEcom = null;
		ModelProduct modelDBekaax = null;
		CompareModelsRequest compareModelRequest = new CompareModelsRequest();
		CompareModelsResponse compareModelsResponse = new CompareModelsResponse(); 
		ConsultEcommerceResponse consultEcommResp = new ConsultEcommerceResponse();
		ProductEcommerceError productError = null;

		logger.info(":::::::::::::::::::::::::::::::::::::: BEFORE MONGO ::::::::::::::::::::::::::::::::::::: ");
		modelDBekaax = productMapper.entityToModel(productRepository.findBySkuId(consultEcommerceRequest.getUpcSku()));
		logger.info(":::::::::::::::::::::::::::::::::::::: AFTER MONGO :::::::::::::::::::::::::::::::::::::: ");
		logger.info(":::: " + modelDBekaax + " ::::");
		
		if (Nullable.isNotNull(modelDBekaax)) {
			modelDBekaax.setEcommerce("ekaax");
			listProduct.add(modelDBekaax);
			
			for (Integer tienda : consultEcommerceRequest.getEcommerce()) {
				logger.info("::::::::::::::::::::: Tienda: " + tienda + ":::::::::::::::::::::::::::");
				try {
					if (tienda.equals(EcommerceEnum.CHEDRAUI.getIdEcommerce())) { // Chedraui = 1
						logger.info("CHEDRAUI ---------------------------------------");
						modelProdEcom = chedrauiService.consultProduct(consultEcommerceRequest.getUpcSku());
					} else if (tienda.equals(EcommerceEnum.WALMART.getIdEcommerce())) { // Walmart = 2
						logger.info("WALMART ---------------------------------------" + consultEcommerceRequest.getUpcSku());
						modelProdEcom = walmartService.consultProduct(consultEcommerceRequest.getUpcSku());
					} else if(tienda.equals(EcommerceEnum.SUPERAMA.getIdEcommerce())) { // Superama = 3
						logger.info("SUPERAMA ---------------------------------------" + consultEcommerceRequest.getUpcSku());
						modelProdEcom = superamaService.consultProduct(consultEcommerceRequest.getMarca(), consultEcommerceRequest.getUpcSku()); 
					}
					
					if(Nullable.isNotNull(modelProdEcom)) {
						compareModelRequest.setModelBase(modelDBekaax);
						compareModelRequest.setModelCompare(modelProdEcom);
						logger.info(compareModelRequest);
						logger.info(":::::::::::::::::::::::::::::::::::::: BEFORE FEIGN ::::::::::::::::::::::::::::::::::::: ");
						compareModelsResponse = feignClientCompareService.compareModel(compareModelRequest);
						logger.info(":::::::::::::::::::::::::::::::::::::: AFTER FEIGN ::::::::::::::::::::::::::::::::::::: ");
						logger.info("feign: "+compareModelsResponse.getModelCompare());
						listProduct.add(compareModelsResponse.getModelCompare());
						
						Integer[] ecommerceId = {tienda};
						saveProductEcommerce(modelProdEcom, ecommerceId);
						
					}else {
						
						saveConsultProductEcommerce(
								consultEcommerceRequest.getUpcSku(), 
								consultEcommerceRequest.getEcommerce(), 
								consultEcommerceRequest.getMarca());
						
						productError = new ProductEcommerceError();
						productError.setSku(consultEcommerceRequest.getUpcSku());
						productError.setMessage("Producto no encontrado en el ecommerce");
						consultEcommResp.addListProductEcommerceError(productError);
					}
				} catch (Exception e) {
					logger.error("ERROR Compare Compare DataBase VS Ecommerce --------------------------------------------");
					logger.error(e);
				}
			} //End For	
			consultEcommResp.setModelProducts(listProduct);
		} else {
			
			saveConsultProductEcommerce(consultEcommerceRequest.getUpcSku(), consultEcommerceRequest.getEcommerce(), consultEcommerceRequest.getMarca());
			// send message error because we dont have product record in ekaax's database
			productError = new ProductEcommerceError();
			productError.setSku(consultEcommerceRequest.getUpcSku());
			productError.setMessage("No Encontrado en la BD de ekaax");
			consultEcommResp.addListProductEcommerceError(productError);
		}
		
		logger.info("END ConsultServiceImpl DataBase VS Ecommerce  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ");
		return consultEcommResp;
	}
	
	/**
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * :::::::::::::::::::::::::::: Compare Ecommerce VS Ecommerce ::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public CompareModelsResponse compareEcommerce(ConsultEcommerceRequest consultEcommerceRequest) {
		logger.info(
				":::::::::::::::: ConsultServiceImpl -> compareEcommerce Compare Ecommerce VS Ecommerce :::::::::::::::::::::::::: "+consultEcommerceRequest.getEcommerce());
		ModelProduct modelProdEcom1 = null;
		ModelProduct modelProdEcom2 = null;
		CompareModelsRequest requestCompareModel = null;
		CompareModelsResponse responseCompareModels = new CompareModelsResponse();
		try {
			//CHEDRAUI
			if ( consultEcommerceRequest.getEcommerce()[0].equals(EcommerceEnum.CHEDRAUI.getIdEcommerce()) ) {
				modelProdEcom1 = chedrauiService.consultProduct(consultEcommerceRequest.getUpcSku());
			} 
			if ( consultEcommerceRequest.getEcommerce()[1].equals(EcommerceEnum.CHEDRAUI.getIdEcommerce()) ) {
				modelProdEcom2 = chedrauiService.consultProduct(consultEcommerceRequest.getUpcSku());
			} 
			
			if ( consultEcommerceRequest.getEcommerce()[0].equals(EcommerceEnum.WALMART.getIdEcommerce()) ) {
				//WALMART
				modelProdEcom1 = walmartService.consultProduct(consultEcommerceRequest.getUpcSku());
			}
			if ( consultEcommerceRequest.getEcommerce()[1].equals(EcommerceEnum.WALMART.getIdEcommerce()) ) {
				modelProdEcom2 = walmartService.consultProduct(consultEcommerceRequest.getUpcSku());
			}
			
			//SUPERAMA
			if ( consultEcommerceRequest.getEcommerce()[0].equals(EcommerceEnum.SUPERAMA.getIdEcommerce()) ) {
				modelProdEcom1 = superamaService.consultProduct(consultEcommerceRequest.getMarca(), consultEcommerceRequest.getUpcSku());
			}
			if ( consultEcommerceRequest.getEcommerce()[1].equals(EcommerceEnum.SUPERAMA.getIdEcommerce()) ) {
				modelProdEcom2 = superamaService.consultProduct(consultEcommerceRequest.getMarca(), consultEcommerceRequest.getUpcSku());
			}
			
			
			logger.info("ModelBase: " + modelProdEcom1);
			logger.info("ModelCompare: " + modelProdEcom2);
			
			if (modelProdEcom1 != null && modelProdEcom2 != null) {
				
				requestCompareModel = new CompareModelsRequest(modelProdEcom1, modelProdEcom2);
				responseCompareModels = feignClientCompareService.compareModelsEcom(requestCompareModel);
				
				saveProductEcommerce(modelProdEcom1, consultEcommerceRequest.getEcommerce());
				saveProductEcommerce(modelProdEcom2, consultEcommerceRequest.getEcommerce());
				
			} else if(modelProdEcom1 != null && modelProdEcom2 == null){
				responseCompareModels.setModelBase(modelProdEcom1);
				responseCompareModels.setModelCompare(null);
				
			} else if(modelProdEcom1 == null && modelProdEcom2 != null){
				responseCompareModels.setModelBase(null);
				responseCompareModels.setModelCompare(modelProdEcom2);
			}
			
		} catch (Exception e) {
			logger.error(
					"ERROR Compare Ecommerce VS Ecommerce ------------------------------------------------------------------");
			logger.error(e);
			logger.error(
					"ERROR Compare Ecommerce VS Ecommerce -----------------------------------------------------------");
		}

		return responseCompareModels;
	}

	/**
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::: Compare CSV VS Ecommerce
	 * :::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public List<ModelProduct> csvConsultEcommService(List<String> listProdCSVString, String[] ecommerce) {
		logger.info("::::::ENTRANDO al csvConsultEcommService::::::");
		List<ModelProduct> listProdEcomm = new ArrayList<>();
		ModelProduct modelProdEcom = null;
		try {
			for (String upcSku : listProdCSVString) {
				logger.info(":SKU:::" + upcSku);

				if (ecommerce[0].equals("1")) { // Chedraui = 1
					logger.info("CHEDRAUI ---------------------------------------" + upcSku);
					modelProdEcom = chedrauiService.consultProduct(upcSku);
				}
				if (ecommerce[0].equals("2")) { // Walmar = 2
					logger.info("WALMART ---------------------------------------" + upcSku);
					modelProdEcom = walmartService.consultProduct(upcSku);
				}
				logger.info(modelProdEcom);
				listProdEcomm.add(modelProdEcom);
			}
		} catch (Exception e) {
			logger.info(
					"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			logger.info(e);
			logger.info(
					"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		logger.info("SALIDA CSVConsult:::" + listProdEcomm);
		return listProdEcomm;
	}

	
	/** ########################### SAVE ##################################
	 * ##################### REFACTOR WITH SPRING AOP #####################
	 * 
	 * this method Save/Update the products
	 *  
	 * @modelProdEcom already is validated that exist in the ecommerce
	 */
	private void saveProductEcommerce(ModelProduct modelProdEcom, Integer[] tiendaArray) {
		logger.info("########################### SAVE ##################################");
		
		ProductEcommerce prodEcomEntity = null;
		ProductEcommerce prodEcomEntityDB = null;
		
		prodEcomEntity = productMapper.modelEcommToEntity(modelProdEcom);
		
		for (Integer tienda : tiendaArray) {
			logger.info("tiendaArray:"+tienda+", modelProdEcom:"+modelProdEcom);
			
			if (EcommerceEnum.CHEDRAUI.getIdEcommerce() == tienda) {
				logger.info("START ::::::::::::::::::: Save product CHEDRAUI :::::::::::::::::::");
				prodEcomEntity.setIdEcommerce(EcommerceEnum.CHEDRAUI.getIdEcommerce());
			} else if (EcommerceEnum.WALMART.getIdEcommerce() == tienda) {
				logger.info("START ::::::::::::::::::: Save product WALMART :::::::::::::::::::");
				prodEcomEntity.setIdEcommerce(EcommerceEnum.WALMART.getIdEcommerce());
			} else if (EcommerceEnum.SUPERAMA.getIdEcommerce() == tienda) {
				logger.info("START ::::::::::::::::::: Save product SUPERAMA :::::::::::::::::::");
				prodEcomEntity.setIdEcommerce(EcommerceEnum.SUPERAMA.getIdEcommerce());
				//Remove the first "0" of the upc because it doesn't need to be
				String upcSkuTmp = prodEcomEntity.getUpcId();
				upcSkuTmp = upcSkuTmp.substring(1, upcSkuTmp.length());
				prodEcomEntity.setUpcId(upcSkuTmp);
				prodEcomEntity.setSkuId(upcSkuTmp);
			}

			prodEcomEntityDB = productEcommerceRepository.findBySkuIdAndIdEcommerce(prodEcomEntity.getUpcId(), prodEcomEntity.getIdEcommerce());
			if (Nullable.isNotNull(prodEcomEntityDB)) {
				prodEcomEntity.setId(prodEcomEntityDB.getId());
			}
			productEcommerceRepository.save(prodEcomEntity);
		}
		
		logger.info("END ::::::::::::::::::: Save product's ecommerce :::::::::::::::::::");
	}
	
	/** ################### CONSULT AND SAVE ##############################
	 * ##################### REFACTOR WITH SPRING AOP #####################
	 * 
	 * this method go to search the product in the ecommerce and save
	 * 
	 */
	private void saveConsultProductEcommerce(String upcSku, Integer[] tiendaArray, String marca) {
		logger.info("################### CONSULT AND SAVE ##############################");
		ProductEcommerce prodEcomEntity = null;
		ProductEcommerce prodEcomEntityDB = null;
		ModelProduct modelProdEcom = null;

		for (Integer tienda : tiendaArray) {
			logger.info("upcSku:"+upcSku+", tiendaArray:"+tienda+", marca:"+marca);

			if (EcommerceEnum.CHEDRAUI.getIdEcommerce() == tienda) {
				modelProdEcom = chedrauiService.consultProduct(upcSku);
				if (Nullable.isNotNull(modelProdEcom)) {
					logger.info("START ::::::::::::::::::: Save product CHEDRAUI :::::::::::::::::::");
					prodEcomEntity = productMapper.modelEcommToEntity(modelProdEcom);
					prodEcomEntity.setIdEcommerce(EcommerceEnum.CHEDRAUI.getIdEcommerce());
				}
			} else if (EcommerceEnum.WALMART.getIdEcommerce() == tienda) {
				modelProdEcom = walmartService.consultProduct(upcSku);
				if (Nullable.isNotNull(modelProdEcom)) {
					logger.info("START ::::::::::::::::::: Save product WALMART :::::::::::::::::::");
					prodEcomEntity = productMapper.modelEcommToEntity(modelProdEcom);
					prodEcomEntity.setIdEcommerce(EcommerceEnum.WALMART.getIdEcommerce());
				}
			} else if (EcommerceEnum.SUPERAMA.getIdEcommerce() == tienda) {
				modelProdEcom = superamaService.consultProduct(marca,upcSku);
				prodEcomEntity = productMapper.modelEcommToEntity(modelProdEcom);
				if (Nullable.isNotNull(modelProdEcom)) {
					logger.info("START ::::::::::::::::::: Save product SUPERAMA :::::::::::::::::::");
					prodEcomEntity.setIdEcommerce(EcommerceEnum.SUPERAMA.getIdEcommerce());
					//Remove the first "0" of the upc because it doesn't need to be
					String upcSkuTmp = prodEcomEntity.getUpcId();
					upcSkuTmp = upcSkuTmp.substring(1, upcSkuTmp.length());
					prodEcomEntity.setUpcId(upcSkuTmp);
					prodEcomEntity.setSkuId(upcSkuTmp);
				}
			}

			if (Nullable.isNotNull(prodEcomEntity)) {
				prodEcomEntityDB = productEcommerceRepository.findProductBySkuId(prodEcomEntity.getSkuId());
				if (Nullable.isNotNull(prodEcomEntityDB)) {
					prodEcomEntity.setId(prodEcomEntityDB.getId());
				}
				logger.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				logger.info(prodEcomEntity);
				logger.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				productEcommerceRepository.save(prodEcomEntity);
			}

		}
		logger.info("END ::::::::::::::::::: Save product's ecommerce :::::::::::::::::::");
	}
	
}
