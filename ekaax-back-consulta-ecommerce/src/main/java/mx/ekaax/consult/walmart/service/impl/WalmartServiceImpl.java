package mx.ekaax.consult.walmart.service.impl;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.ekaax.consult.api.ApiWalmartUrl;
import mx.ekaax.consult.api.ConsumeApi;
import mx.ekaax.consult.walmart.service.IWalmartService;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.utils.general.helper.EcommerceEnum;

@Service
public class WalmartServiceImpl implements IWalmartService{
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	private ApiWalmartUrl apiWalmartUrl;

	@Override
	public ModelProduct consultProduct(String upcSku) {
		
		ObjectMapper mapper = new ObjectMapper();
        ModelProduct modelProduct = null;
		try {
			
			JsonNode rootJsonApi1 = mapper.readTree(
					ConsumeApi.consumeApiGeneral(apiWalmartUrl.getWalmartApi1().replaceAll(":upcSku", upcSku)).getBody()
					);
			
			if(rootJsonApi1.has("skuDisplayNameText")) {
				modelProduct = new ModelProduct();
				modelProduct.setEcommerce(EcommerceEnum.WALMART.getNameEcommerce());
				modelProduct.setUpcId(upcSku);
				modelProduct.setSkuId(upcSku);
				modelProduct.setTitle(rootJsonApi1.path("skuDisplayNameText").asText());
				modelProduct.setDescription(rootJsonApi1.path("longDescription").asText());
				modelProduct.setPrice(BigDecimal.valueOf(rootJsonApi1.path("basePrice").asDouble()));
				modelProduct.setPicture("https://super.walmart.com.mx/images/product-images/img_medium/00"+upcSku+"m.jpg");
			}
			
		} catch (Exception e) {
			logger.error("#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*");
			logger.error("START ERROR al recuperar el producto de WALMART o NO se encuentra en el Ecommerce");
			//logger.error(e);
			logger.error("ERROR END #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*");
			modelProduct = null;
		}
		return modelProduct;
		
	}

}
