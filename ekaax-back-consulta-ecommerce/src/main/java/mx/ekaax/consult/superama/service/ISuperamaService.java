package mx.ekaax.consult.superama.service;

import mx.ekaax.model.ModelProduct;

public interface ISuperamaService {
	
	public ModelProduct consultProduct(String marca, String upcSkuSuperama);

}
