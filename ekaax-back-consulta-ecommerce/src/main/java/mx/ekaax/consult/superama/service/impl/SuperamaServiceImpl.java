package mx.ekaax.consult.superama.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import mx.ekaax.consult.api.ApiSuperamaUrl;
import mx.ekaax.consult.api.ConsumeApi;
import mx.ekaax.consult.superama.service.ISuperamaService;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.utils.general.helper.EcommerceEnum;

@Service
public class SuperamaServiceImpl implements ISuperamaService {
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	private ApiSuperamaUrl apiSuperamaUrl;
	
	@Override
	public ModelProduct consultProduct(String marca, String upcSkuSuperama) {
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		ObjectMapper mapper = new ObjectMapper();
        ModelProduct modelProduct = null;
        ArrayNode productArray = null;
        String upcSku = "0".concat(upcSkuSuperama);
        
		try {
			modelProduct = new ModelProduct();
			JsonNode rootJsonApi1 = mapper.readTree(
					ConsumeApi.consumeApiGeneral(apiSuperamaUrl.getSuperamaApi1().replaceAll(":marca", marca)).getBody()
					);
			
			if(rootJsonApi1.has("Products") && !rootJsonApi1.path("Products").isNull()) {
				
				productArray = (ArrayNode) rootJsonApi1.path("Products");

				Optional<JsonNode> productOptional = null;

				Stream<JsonNode> streamJsonNode = StreamSupport.stream(productArray.spliterator(), false);
				productOptional = streamJsonNode.filter(p -> p.path("Upc").asText().equals(upcSku)).findFirst();
				
				JsonNode productNode = productOptional.get();
				
				modelProduct.setEcommerce(EcommerceEnum.SUPERAMA.getNameEcommerce());
				modelProduct.setSkuId(productNode.path("Upc").asText());
				modelProduct.setUpcId(productNode.path("Upc").asText());
				//modelProduct.setTitle(productNode.path("DescriptionDisplay").asText());
				modelProduct.setPicture("https://www.superama.com.mx/"+productNode.path("ImageUrl").asText());
				
				String departmentName = productNode.path("DepartmentName").asText();
				String familyName = productNode.path("FamilyName").asText();
				String lineName = productNode.path("LineName").asText();
				String seoProductUrlName = productNode.path("SeoProductUrlName").asText();

				String urlApi2 = apiSuperamaUrl.getSuperamaApi2().replaceAll(":DepartmentName", departmentName)
						.replaceAll(":FamilyName", familyName).replaceAll(":LineName", lineName)
						.replaceAll(":SeoProductUrlName", seoProductUrlName).replaceAll(":upcSku", upcSku);

				String htmlSuperama = ConsumeApi.consumeApiGeneral(urlApi2).getBody();
				
				String jsonString = null;
				
				URL url = new URL(urlApi2);				
				InputStream stream = new ByteArrayInputStream(htmlSuperama.getBytes(StandardCharsets.UTF_8));
				BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
				String htmlText;
				while ((htmlText = reader.readLine()) != null) {
					if(htmlText.contains("var model = {\"")) {
						jsonString = htmlText.trim().replace("var model = ", "").replaceAll(";", "");
					}
				}
				reader.close();
				
				JsonNode rootJsonApi2 = mapper.readTree(jsonString);
				modelProduct.setTitle(rootJsonApi2.path("Description").asText());
				modelProduct.setPrice(new BigDecimal(rootJsonApi2.path("Price").asText()));
				modelProduct.setDescription(rootJsonApi2.path("Details").asText());
				modelProduct.setSummary(rootJsonApi2.path("Ingredients").asText());
				
				
			}
			logger.info(modelProduct.toString());	
			logger.info("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			
		} catch (Exception e) {
			logger.error("#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*");
			logger.error("START ERROR al recuperar el producto de SUPERAMA o no se encuentra");
			//logger.error(e);
			logger.error("ERROR END #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*");
			modelProduct = null;
		}
		logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		return modelProduct;
	}

}
