package mx.ekaax.consult.api;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "superama")
public class ApiSuperamaUrl {
	
	private String superamaApi1;
	private String superamaApi2;
	
	public ApiSuperamaUrl() {
		super();
	}

	public ApiSuperamaUrl(String superamaApi1, String superamaApi2) {
		super();
		this.superamaApi1 = superamaApi1;
		this.superamaApi2 = superamaApi2;
	}

	public String getSuperamaApi1() {
		return superamaApi1;
	}

	public void setSuperamaApi1(String superamaApi1) {
		this.superamaApi1 = superamaApi1;
	}

	public String getSuperamaApi2() {
		return superamaApi2;
	}

	public void setSuperamaApi2(String superamaApi2) {
		this.superamaApi2 = superamaApi2;
	}

	
}
