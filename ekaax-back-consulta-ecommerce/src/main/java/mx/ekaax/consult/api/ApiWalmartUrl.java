package mx.ekaax.consult.api;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "walmart")
public class ApiWalmartUrl {
	
	private String walmartApi1;
	private String walmartApi2;
	
	public ApiWalmartUrl() {
		super();
	}

	public ApiWalmartUrl(String walmartApi1, String walmartApi2) {
		super();
		this.walmartApi1 = walmartApi1;
		this.walmartApi2 = walmartApi2;
	}

	public String getWalmartApi1() {
		return walmartApi1;
	}

	public void setWalmartApi1(String walmartApi1) {
		this.walmartApi1 = walmartApi1;
	}

	public String getWalmartApi2() {
		return walmartApi2;
	}

	public void setWalmartApi2(String walmartApi2) {
		this.walmartApi2 = walmartApi2;
	}

}
