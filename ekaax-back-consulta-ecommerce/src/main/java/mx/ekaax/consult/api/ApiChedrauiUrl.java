package mx.ekaax.consult.api;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "chedraui")
public class ApiChedrauiUrl {

	private String chedrauiApi1;
	private String chedrauiApi2;

	public ApiChedrauiUrl() {
		super();
	}

	public ApiChedrauiUrl(String chedrauiApi1, String chedrauiApi2) {
		super();
		this.chedrauiApi1 = chedrauiApi1;
		this.chedrauiApi2 = chedrauiApi2;
	}

	public String getChedrauiApi1() {
		return chedrauiApi1;
	}

	public void setChedrauiApi1(String chedrauiApi1) {
		this.chedrauiApi1 = chedrauiApi1;
	}

	public String getChedrauiApi2() {
		return chedrauiApi2;
	}

	public void setChedrauiApi2(String chedrauiApi2) {
		this.chedrauiApi2 = chedrauiApi2;
	}

}
