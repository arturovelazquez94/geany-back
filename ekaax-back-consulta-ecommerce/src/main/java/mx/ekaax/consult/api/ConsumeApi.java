package mx.ekaax.consult.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ConsumeApi {
	private static final Log logger = LogFactory.getLog(ConsumeApi.class);
	
	public static ResponseEntity<String> consumeApiGeneral(String url) {
		
		try {
			CloseableHttpClient httpClient = HttpClients.custom()
			        .setSSLHostnameVerifier(new NoopHostnameVerifier())
			        	.build();
		    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		    requestFactory.setHttpClient(httpClient);
		    
		    ResponseEntity<String> response = new RestTemplate(requestFactory).exchange(url, HttpMethod.GET, null, String.class);
		    
		    return response;
		} catch (Exception e) {
			logger.error("START ERROR ConsumeApi #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*");
			logger.error(e);
			logger.error("END ERROR ConsumeApi #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*");
			return null;
		}
	    
	}

}
