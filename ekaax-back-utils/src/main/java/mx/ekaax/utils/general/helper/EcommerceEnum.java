package mx.ekaax.utils.general.helper;

import static mx.ekaax.utils.general.helper.EcommerceEnum.Constants.*;

public enum EcommerceEnum {

	CHEDRAUI(CONSULT_ECOMMERCE_TYPE_CHEDRAUI, CONSULT_ECOMMERCE_NAME_CHEDRAUI), WALMART(CONSULT_ECOMMERCE_TYPE_WALMART,
			CONSULT_ECOMMERCE_NAME_WALMART), SUPERAMA(CONSULT_ECOMMERCE_TYPE_SUPERAMA,
					CONSULT_ECOMMERCE_NAME_SUPERAMA), UNKNOWN(CONSULT_ECOMMERCE_TYPE_UNKWNOWN,
							CONSULT_ECOMMERCE_NAME_UNKNOWN);

	private final int idEcommerce;

	private final String nameEcommerce;

	private EcommerceEnum(Integer idEcommerce, String nameEcommerce) {
		this.idEcommerce = idEcommerce;
		this.nameEcommerce = nameEcommerce;
	}

	public int getIdEcommerce() {
		return idEcommerce;
	}

	public String getNameEcommerce() {
		return nameEcommerce;
	}

	public static class Constants {

		// ----------------------------------
		// ECOMMERCE NAME----------------------------------------//
		public static final String CONSULT_ECOMMERCE_NAME_UNKNOWN = "UNKNOWN";

		public static final String CONSULT_ECOMMERCE_NAME_CHEDRAUI = "Chedraui";

		public static final String CONSULT_ECOMMERCE_NAME_WALMART = "Walmart";

		public static final String CONSULT_ECOMMERCE_NAME_SUPERAMA = "Superama";

		// ----------------------------------
		// ECOMMERCE TYPE----------------------------------------//
		public static final Integer CONSULT_ECOMMERCE_TYPE_UNKWNOWN = 0;

		public static final Integer CONSULT_ECOMMERCE_TYPE_CHEDRAUI = 1;

		public static final Integer CONSULT_ECOMMERCE_TYPE_WALMART = 2;

		public static final Integer CONSULT_ECOMMERCE_TYPE_SUPERAMA = 3;

	}

	public static EcommerceEnum getEcommerce(String type) {

		EcommerceEnum ecommerce = null;

		switch (type) {
		case "1":

			ecommerce = EcommerceEnum.CHEDRAUI;
			break;

		case "2":

			ecommerce = EcommerceEnum.WALMART;
			break;
		case "3":

			ecommerce = EcommerceEnum.SUPERAMA;
			break;

		default:
			ecommerce = EcommerceEnum.UNKNOWN;
			break;
		}
		return ecommerce;

	}

}
