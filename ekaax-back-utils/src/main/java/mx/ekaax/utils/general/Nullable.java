package mx.ekaax.utils.general;

import java.util.List;
import java.util.Optional;

public final class Nullable {

	public static final boolean isNull(Object obj) {
		return !Optional.ofNullable(obj).isPresent();
	}

	public static final boolean isTrue(Object obj) {
		if (Optional.ofNullable(obj).isPresent()) {
			return (Boolean) obj;
		}

		return false;
	}

	public static final boolean isNotNull(Object obj) {
		return Optional.ofNullable(obj).isPresent();
	}

	public static final boolean isNullOrWhitespace(Object obj) {

		if (!Optional.ofNullable(obj).isPresent()) {
			return true;
		}

		String s = String.valueOf(obj);
		if (s.trim().isEmpty()) {
			return true;
		}

		return false;
	}

	@SuppressWarnings("rawtypes")
	public static final boolean isNullOrEmpty(Object obj) {

		if (!Optional.ofNullable(obj).isPresent()) {
			return true;
		}

		if (obj instanceof List) {
			if (((List) obj).isEmpty()) {
				return true;
			}
		} else if (obj instanceof Object[]) {
			if (((Object[]) obj).length == 0) {
				return true;
			}
		}

		return false;
	}

	public static final <T> T retValue(Object obj, T value, Class<T> clazz) {
		try {
			return isNullOrWhitespace(obj) ? value : clazz.cast(obj);
		} catch (ClassCastException e) {
			return null;
		}
	}
}
