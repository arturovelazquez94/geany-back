
package mx.ekaax.utils.general.helper;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ControllerHelper {

	public static <T> ResponseEntity<T> responseOK(T body) {
		return new ResponseEntity<T>(body, HttpStatus.OK);
	}

	public static <T> ResponseEntity<T> responseCreated(T body) {
		return new ResponseEntity<T>(body, HttpStatus.CREATED);
	}
	
	public static <T> ResponseEntity<T> responseNotFound(T body) {
		return new ResponseEntity<T>(body, HttpStatus.NOT_FOUND);
	}
	
	public static <T> ResponseEntity<T> responseBadRequest(T body) {
		return new ResponseEntity<T>(body, HttpStatus.BAD_REQUEST);
	}
	
	public static <T> ResponseEntity<T> responseForbidden(T body) {
		return new ResponseEntity<T>(body, HttpStatus.FORBIDDEN);
	}
	
}
