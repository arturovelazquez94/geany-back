package mx.ekaax.utils.general.helper;

import java.util.Date;

public enum ReportCompareEnum {

	CSV(Constants.CSV_TYPE), DATABASE(Constants.DATABASE_TYPE), ECOMMERCE(Constants.ECOMMERCE_TYPE);

	private Integer compareType;


	private ReportCompareEnum(Integer compareType) {

		this.compareType = compareType;
	}

	public static class Constants {

		// ECOMMERCE TYPE----------------------------------------//

		public static final Integer CSV_TYPE = 1;

		public static final Integer DATABASE_TYPE = 2;

		public static final Integer ECOMMERCE_TYPE = 3;

	}

	public Integer getCompareType() {
		return compareType;
	}

	public void setCompareType(Integer compareType) {
		this.compareType = compareType;
	}


}
