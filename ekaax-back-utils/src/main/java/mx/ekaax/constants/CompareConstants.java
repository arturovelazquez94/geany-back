package mx.ekaax.constants;

public class CompareConstants {

	public static final Integer COMPARE_PRODUCT_OK = 1;

	public static final Integer COMPARE_PRODUCT_MISTAKEN = 2;

	public static final Integer COMPARE_PRODUCT_EMPTY = 3;

	public static final Integer COMPARE_PRODUCT_FIELD_NOT_MATCH = 2;

	public static final Integer COMPARE_PRODUCT_FIELD_EMPTY = 3;

}
