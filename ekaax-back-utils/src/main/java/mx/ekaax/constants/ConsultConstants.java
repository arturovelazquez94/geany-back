package mx.ekaax.constants;

public class ConsultConstants {


	// FIELDS----------------------------------------//

	public static final String CONSULT_FIELD_TITLE = "Titulo";

	public static final String CONSULT_FIELD_DESCRIPTION = "Descripcion";

	public static final String CONSULT_FIELD_PRICE = "Precio";

}
