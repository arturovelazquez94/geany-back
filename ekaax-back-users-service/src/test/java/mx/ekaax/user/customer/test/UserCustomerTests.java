package mx.ekaax.user.customer.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import mx.ekaax.user.customer.controller.impl.UserCustomerControllerImpl;
import mx.ekaax.user.customer.domain.UserCustomer;
import mx.ekaax.user.customer.mapper.UserCustomerMapper;
import mx.ekaax.user.customer.request.Animal;
import mx.ekaax.user.customer.response.UserCustomerResponse;
import mx.ekaax.user.customer.service.impl.UserCustomerServiceWrapperImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserCustomerControllerImpl.class, secure = false)
public class UserCustomerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserCustomerServiceWrapperImpl userCustomerWrapperImpl;

	@MockBean
	private UserCustomerMapper userCustomerMapper;

	private JacksonTester<UserCustomerResponse> jsonCustomerUser;

	@Before
	public void setup() {
		// Initializes the JacksonTester
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void testCustomer() throws Exception {

		// Given
		UserCustomer userCustomer = new UserCustomer();
		userCustomer.setId(UUID.fromString("978f2773-e43a-4d28-bb30-3434be0ede0c"));
		userCustomer.setAge(1);
		userCustomer.setEnabled(true);
		userCustomer.setLastName("Velazquez");
		userCustomer.setPhoneNumber("5511454144");
		userCustomer.setName("Carlos");
		UserCustomerResponse userCustomerResponse = userCustomerMapper.entityToResponse(userCustomer);

		UUID id = UUID.fromString("978f2773-e43a-4d28-bb30-3434be0ede0c");
		Integer idNumber = 1;
		BDDMockito.given(userCustomerWrapperImpl.getById(idNumber)).willReturn(userCustomerResponse);

		Integer numero = 1;
		Animal animal = new Animal();
		animal.setId("2");
		animal.setName("perro");

		UserCustomerResponse userResponse = new UserCustomerResponse();
		userResponse.setName("Alfonse");
		System.out.println("USER CUSTOMER MAPPER:::" + userCustomerResponse);

		BDDMockito.given(userCustomerWrapperImpl.getByIdExample(numero)).willReturn(userCustomerResponse);

		MockHttpServletResponse response = mockMvc
				.perform(MockMvcRequestBuilders.get("/users/example/{id}", numero).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		BDDMockito.verify(userCustomerWrapperImpl).getByIdExample(numero);
		System.out.println("JACKSON___________________::" + jsonCustomerUser.toString());
		System.out.println("Response:::::::::" + response.getContentAsString());
		// assertThat(response.getContentAsString()).isEqualTo(jsonCustomerUser.write(userCustomerResponse).getJson());
	}
}
