package mx.ekaax.user.customer.test;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import mx.ekaax.user.customer.controller.impl.ExampleController;
import mx.ekaax.user.customer.domain.UserCustomer;
import mx.ekaax.user.customer.mapper.UserCustomerMapper;
import mx.ekaax.user.customer.resource.UserCustomerResource;
import mx.ekaax.user.customer.service.impl.ExampleService;
import mx.ekaax.user.customer.service.impl.UserCustomerServiceWrapperImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ExampleController.class, secure = false)
public class ExampleTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ExampleService exampleService;

	@Before
	public void setUp() throws Exception {
		// mockMvc = MockMvcBuilders.standaloneSetup(customerTests)
		// .setCustomArgumentResolvers(new
		// PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	public void testUserCustomer() throws Exception {

		Integer id = 6;
		BDDMockito.given(exampleService.getNamer(id)).willReturn("Mentive");
		mockMvc.perform(MockMvcRequestBuilders.get(ExampleController.URL, id).accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("Ekaax"));

	}
}