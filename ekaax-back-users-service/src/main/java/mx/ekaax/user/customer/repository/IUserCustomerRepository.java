package mx.ekaax.user.customer.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.ekaax.user.customer.domain.UserCustomer;

@Repository
public interface IUserCustomerRepository extends JpaRepository<UserCustomer, UUID> {

	Optional<UserCustomer> findById(UUID id);

}
