package mx.ekaax.user.customer.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ekaax.user.customer.service.impl.ExampleService;

@RestController
@RequestMapping("/mentive")
public class ExampleController {

	public static final String URL = "/mentive/{id}";

	@Autowired
	private ExampleService exampleService;

	@GetMapping("/{id}")
	public String getName(@PathVariable Integer id) {
		return exampleService.getNamer(id);
	}
}
