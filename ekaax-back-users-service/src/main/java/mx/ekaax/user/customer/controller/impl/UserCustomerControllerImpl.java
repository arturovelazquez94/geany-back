package mx.ekaax.user.customer.controller.impl;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import mx.ekaax.user.customer.controller.IUserCustomerServiceController;
import mx.ekaax.user.customer.request.Animal;
import mx.ekaax.user.customer.request.UserCustomerRequest;
import mx.ekaax.user.customer.resource.UserCustomerResource;
import mx.ekaax.user.customer.response.UserCustomerResponse;
import mx.ekaax.user.customer.service.IUserCustomerServiceWrapper;

@Component
public class UserCustomerControllerImpl implements IUserCustomerServiceController {

	@Autowired
	private IUserCustomerServiceWrapper userCustomerServiceWrapper;

	@Override
	public ResponseEntity<?> create(@RequestBody UserCustomerRequest userCustomerRequest) {

		final UserCustomerResponse userCusomerResponse = userCustomerServiceWrapper.create(userCustomerRequest);

		final URI uri = MvcUriComponentsBuilder.fromController(getClass()).path("/{id}")
				.buildAndExpand(userCusomerResponse.getId()).toUri();
		return ResponseEntity.created(uri).body(new UserCustomerResource(userCusomerResponse));

	}

	@Override
	public ResponseEntity<?> getAll(Pageable pageable) {
		final List<UserCustomerResource> usersCustomerResponse = userCustomerServiceWrapper.getAll(pageable).stream()
				.map(UserCustomerResource::new).collect(Collectors.toList());

		final Resources<UserCustomerResource> usersResources = new Resources<>(usersCustomerResponse);
		final String uriString = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();

		usersResources.add(new Link(uriString, "self"));

		return ResponseEntity.ok(usersResources);
	}

	@Override
	public UserCustomerResponse getById(@PathVariable Integer id) {
		// UserCustomerResource userResource = new
		// UserCustomerResource(userCustomerServiceWrapper.getById(id));
		return userCustomerServiceWrapper.getById(id);
	}

	@Override
	public ResponseEntity<?> put(@PathVariable UUID id, @RequestBody UserCustomerRequest userCustomerRequest) {
		final UserCustomerResource userResource = new UserCustomerResource(
				userCustomerServiceWrapper.update(userCustomerRequest, id));
		final URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();

		return ResponseEntity.created(uri).body(userResource);
	}

	@Override
	public ResponseEntity<?> delete(@PathVariable UUID id) {
		userCustomerServiceWrapper.deleteById(id);
		return ResponseEntity.noContent().build();
	}

	@Override
	public UserCustomerResponse getByIdExample(@PathVariable Integer id) {
		return userCustomerServiceWrapper.getByIdExample(id);
	}

}
