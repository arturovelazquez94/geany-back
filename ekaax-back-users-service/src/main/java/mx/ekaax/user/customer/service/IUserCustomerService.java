package mx.ekaax.user.customer.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;

import mx.ekaax.user.customer.domain.UserCustomer;

/**
 * 
 * @author Arturo Velázquez Vargas
 *
 */
public interface IUserCustomerService {
	/**
	 * 
	 * @param id
	 * @return
	 */
	UserCustomer findById(UUID id);

	/**
	 * 
	 * @param user
	 * @param id
	 * @return
	 */
	UserCustomer update(UserCustomer user, UUID id);

	/**
	 * 
	 * @param Id
	 */
	void delete(UUID Id);

	/**
	 * 
	 * @param page
	 * @return
	 */
	List<UserCustomer> findAll(Pageable page);

	/**
	 * 
	 * @param userCustomer
	 * @return
	 */
	UserCustomer save(UserCustomer userCustomer);

}
