package mx.ekaax.user.customer.controller;

import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ekaax.user.customer.request.Animal;
import mx.ekaax.user.customer.request.UserCustomerRequest;
import mx.ekaax.user.customer.response.UserCustomerResponse;

@RestController
@RequestMapping(value = "/users")
public interface IUserCustomerServiceController {

	@PostMapping
	ResponseEntity<?> create(UserCustomerRequest userCustomerRequest);

	@GetMapping(produces = "application/json")
	ResponseEntity<?> getAll(Pageable pageable);

	@GetMapping("/{id}")
	UserCustomerResponse getById(Integer id);

	@GetMapping(value = "/example/{id}")
	UserCustomerResponse getByIdExample(Integer id);

	@PutMapping("/{id}")
	ResponseEntity<?> put(UUID id, UserCustomerRequest userCustomerRequest);

	@DeleteMapping("/{id}")
	ResponseEntity<?> delete(UUID id);

}
