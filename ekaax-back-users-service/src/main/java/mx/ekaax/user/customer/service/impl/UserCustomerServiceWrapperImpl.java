package mx.ekaax.user.customer.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import mx.ekaax.user.customer.domain.UserCustomer;
import mx.ekaax.user.customer.mapper.UserCustomerMapper;
import mx.ekaax.user.customer.request.Animal;
import mx.ekaax.user.customer.request.UserCustomerRequest;
import mx.ekaax.user.customer.response.UserCustomerResponse;
import mx.ekaax.user.customer.service.IUserCustomerService;
import mx.ekaax.user.customer.service.IUserCustomerServiceWrapper;

@Service
public class UserCustomerServiceWrapperImpl implements IUserCustomerServiceWrapper {

	@Autowired
	private IUserCustomerService userCustomerService;

	@Autowired
	private UserCustomerMapper userCustomerMapper;

	@Override
	public List<UserCustomerResponse> getAll(Pageable page) {

		return userCustomerMapper.entityToResponse(userCustomerService.findAll(page));
	}

	@Override
	public UserCustomerResponse getById(Integer id) {
		// return userCustomerMapper.entityToResponse(userCustomerService.findById(id));
		return new UserCustomerResponse();
	}

	@Override
	public void deleteById(UUID id) {
		userCustomerService.delete(id);
	}

	@Override
	public UserCustomerResponse create(UserCustomerRequest customerRequest) {
		UserCustomer userCustomer = userCustomerMapper.requestToEntity(customerRequest);
		return userCustomerMapper.entityToResponse(userCustomerService.save(userCustomer));
	}

	@Override
	public UserCustomerResponse update(UserCustomerRequest customerRequest, UUID id) {

		UserCustomer entityUpdate = userCustomerMapper.requestToEntity(customerRequest);
		UserCustomerResponse userResponse = userCustomerMapper
				.entityToResponse(userCustomerService.update(entityUpdate, id));

		return userResponse;
	}

	@Override
	public UserCustomerResponse getByIdExample(Integer entero) {

		return new UserCustomerResponse();
	}

}
