package mx.ekaax.user.customer.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;

import mx.ekaax.user.customer.request.Animal;
import mx.ekaax.user.customer.request.UserCustomerRequest;
import mx.ekaax.user.customer.response.UserCustomerResponse;

public interface IUserCustomerServiceWrapper {

	List<UserCustomerResponse> getAll(Pageable page);

	UserCustomerResponse getById(Integer id);

	void deleteById(UUID id);

	UserCustomerResponse create(UserCustomerRequest customerRequest);

	UserCustomerResponse update(UserCustomerRequest customerRequest, UUID id);

	UserCustomerResponse getByIdExample(Integer entero);

}
