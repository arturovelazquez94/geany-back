package mx.ekaax.user.customer.helper;

import mx.ekaax.user.customer.domain.UserCustomer;
import mx.ekaax.utils.general.Nullable;

public class UserCustomerHelper {

	public static class RepositoryHelper {

		public static void setUpdateProperties(UserCustomer entity, UserCustomer newEntity) {

			if (Nullable.isNotNull(newEntity)) {

				if (Nullable.isNotNull(newEntity.getName())) {

					entity.setName(newEntity.getName());
				} else if (Nullable.isNotNull(newEntity.getLastName())) {

					entity.setLastName(newEntity.getLastName());
				} else if (Nullable.isNotNull(newEntity.getAge())) {

					entity.setAge(newEntity.getAge());
				} else if (Nullable.isNotNull(newEntity.getPhoneNumber())) {

					entity.setPhoneNumber(newEntity.getPhoneNumber());
				} else if (Nullable.isNotNull(newEntity.getUserStatus())) {

					entity.setUserStatus(newEntity.getUserStatus());
				} else if (Nullable.isNotNull(newEntity.getUserType())) {

					entity.setUserType(newEntity.getUserType());
				}

			}
		}

	}

}
