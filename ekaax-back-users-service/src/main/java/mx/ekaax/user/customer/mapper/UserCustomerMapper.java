package mx.ekaax.user.customer.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import mx.ekaax.user.customer.domain.UserCustomer;
import mx.ekaax.user.customer.request.UserCustomerRequest;
import mx.ekaax.user.customer.response.UserCustomerResponse;

@Mapper(componentModel = "spring")
public interface UserCustomerMapper {

	UserCustomer requestToEntity(UserCustomerRequest userRequest);

	List<UserCustomerResponse> entityToResponse(List<UserCustomer> userEntity);

	UserCustomerResponse entityToResponse(UserCustomer userEntity);

}
