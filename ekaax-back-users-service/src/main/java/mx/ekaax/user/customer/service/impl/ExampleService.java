package mx.ekaax.user.customer.service.impl;

import org.springframework.stereotype.Service;

@Service
public class ExampleService {

	public String getNamer(Integer id) {

		return id > 0 ? "Pedro" : "Juan";
	}

}
