package mx.ekaax.user.customer.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import mx.ekaax.user.customer.domain.UserCustomer;
import mx.ekaax.user.customer.helper.UserCustomerHelper;
import mx.ekaax.user.customer.repository.IUserCustomerRepository;
import mx.ekaax.user.customer.service.IUserCustomerService;

@Service
public class UserCustomerServiceImpl implements IUserCustomerService {

	@Autowired
	private IUserCustomerRepository userRepository;

	@Override
	public UserCustomer findById(UUID id) {

		Optional<UserCustomer> optionalUser = userRepository.findById(id);

		if (!optionalUser.isPresent()) {
			// Temporal , CORRECT 404
			throw new RuntimeException("User nos exists");
		}
		return optionalUser.get();
	}

	@Override
	public UserCustomer update(UserCustomer user, UUID id) {

		Optional<UserCustomer> optionalUser = userRepository.findById(id);

		if (!optionalUser.isPresent()) {
			// Temporal , CORRECT 404
			throw new RuntimeException("User nos exists");
		}

		UserCustomer userEntityUpdate = optionalUser.get();
		UserCustomerHelper.RepositoryHelper.setUpdateProperties(userEntityUpdate, user);

		userRepository.save(userEntityUpdate);
		return userEntityUpdate;
	}

	@Override
	public void delete(UUID id) {
		userRepository.deleteById(id);
	}

	@Override
	public List<UserCustomer> findAll(Pageable page) {
		return userRepository.findAll(page).getContent();
	}

	@Override
	public UserCustomer save(UserCustomer userCustomer) {
		return userRepository.save(userCustomer);
	}

}
