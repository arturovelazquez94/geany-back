package mx.ekaax.user.customer.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * 
 * @author Arturo Velazquez
 *
 */
@Data
@Entity
@Table(name = "users_customer",schema="geany_users")
public class UserCustomer {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "UUID")
	private UUID id;

	@Column(name = "name")
	private String name;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "age")
	private Integer age;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "enabled")
	private boolean enabled;

	@Column(name = "user_type")
	private Integer userType;

	@Column(name = "user_status")
	private Integer userStatus;
}
