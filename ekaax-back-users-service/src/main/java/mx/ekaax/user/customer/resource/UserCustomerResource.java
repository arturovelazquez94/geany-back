package mx.ekaax.user.customer.resource;

import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import lombok.Data;
import lombok.Getter;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import mx.ekaax.user.customer.controller.impl.UserCustomerControllerImpl;
import mx.ekaax.user.customer.domain.UserCustomer;
import mx.ekaax.user.customer.response.UserCustomerResponse;

@Data
public class UserCustomerResource extends ResourceSupport {
	@Getter
	private final UserCustomerResponse userCustomer;

	public UserCustomerResource(UserCustomerResponse userCustomer) {

		this.userCustomer = userCustomer;
		final UUID id = userCustomer.getId();
//		add(linkTo(UserCustomerControllerImpl.class).withRel("userCustomers"));
//		add(linkTo(methodOn(UserCustomerControllerImpl.class).getById(id)).withSelfRel());
	}

}
