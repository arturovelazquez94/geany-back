package mx.ekaax.user.customer.response;


import java.util.UUID;


import lombok.Data;

@Data
public class UserCustomerResponse {

	private UUID id;

	private String name;

	private String lastName;

	private Integer age;

	private String phoneNumber;

	private boolean enabled;

	private Integer userType;

	private Integer userStatus;

}
