package mx.ekaax.user.customer.request;

import lombok.Data;

@Data
public class Animal {

	private String name;

	private String id;

}
